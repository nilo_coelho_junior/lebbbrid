import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import thunk from 'redux-thunk'

import App from './components/app'
import ListShots from './containers/listShots'
import Shot from './containers/shot'
import reducers from './reducers'

import { HOME, SHOT } from './constants/routeTypes'

const store = createStore(reducers, applyMiddleware(thunk))

require('../style/style.scss')

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path={HOME} component={App}>
        <IndexRoute component={ListShots} />
        <Route path={SHOT} component={Shot} />
      </Route>
    </Router>
  </Provider>
  , document.querySelector('#app'))
