import React, {Component} from 'react'

export default class ListShotItem extends Component {
  render () {
    return (
      <li className='col-sm-3 shot-container'>
        <div className='shot'>
          <div className='wrapper'>
            <div className='preview'>
              <a href='#' className='thumbnail'>
                <img src='https://cdn.dribbble.com/users/161174/screenshots/2858032/carla-soriani-feliz-happy-dribbble-2_1x.gif' />
              </a>
            </div>
            <div className='description'>
              <strong className='title'>Be Happy</strong>
              <span className='text'>I'm always feeling happy. Even when things are not perfect.</span>
              <span className='date'>July 25, 2016</span>
            </div>
          </div>
        </div>
        <h5 className='col-sm-12 shot-author'>
          <img src='https://cdn.dribbble.com/users/161174/avatars/mini/f0e555a9c8d7e871427d3a5597466e89.png?1493460023' className='img-circle' />
          <span> Carla Soriani </span>
        </h5>
      </li>
    )
  }
}
