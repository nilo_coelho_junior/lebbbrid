import React, {Component} from 'react'
import Search from '../containers/search'

export default class App extends Component {

  constructor (props) {
    super(props)

    this.previousChildren = ''
    this.isModal = false
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.location.state && nextProps.location.state.modal) {
      this.previousChildren = this.props.children
      this.isModal = true
    }
  }

  render () {
    return (
      <div>
        <header className='jumbotron'>
          <div className='container'>
            <h1 className='text-center'>Lebbbrid</h1>
            <br />
            <Search />
          </div>
        </header>
        <main role='main' className='container'>
          {
            this.isModal ? this.previousChildren : this.props.children
          }
        </main>
      </div>
    )
  }
}
