import React, {Component} from 'react'
import ListShotItem from './listShotsItem'

const list = Array.from(Array(100).keys())

const ListShots = () => (
  <ol className='list-unstyled'>
    {
      list.map(l =>
        <ListShotItem />
      )
    }
  </ol>
)



export default ListShots
