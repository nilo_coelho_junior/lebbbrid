import * as types from '../constants/actionTypes'
import axios from 'axios'

let api = axios.create({
  baseURL: 'http://localhost:9000'
})

export function loadingList (status) {
  return {type: types.LIST_SHOTS_LOADING, payload: status}
}

export function receiveShots (list) {
  return {type: types.LIST_SHOTS_RECEIVE, payload: list}
}

export function getShots (params = '') {
  let url = '/shots' + params
  return dispatch => {
    dispatch(loadingList(true))
    api.get(url).then(response => {
      dispatch(receiveShots(response.data))
      dispatch(loadingList(false))
    })
  }
}
