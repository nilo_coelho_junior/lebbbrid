import { LIST_SHOTS_LOADING, LIST_SHOTS_ERROR, LIST_SHOTS_RECEIVE } from '../constants/actionTypes'

const initalState = {
  loadingList: true,
  loadingShot: false,
  error: null,
  data: []
}

export default function reducerShots (state = initalState, action) {
  let newState = Object.assign({}, state)
  switch (action.type) {
    case LIST_SHOTS_LOADING:
      newState.loadingList = action.payload
      break
    case LIST_SHOTS_ERROR:
      newState.error = action.payload
      break
    case LIST_SHOTS_RECEIVE:
      newState.data = state.data.concat(action.payload)
  }
  return newState
}
