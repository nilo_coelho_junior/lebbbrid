import { combineReducers } from 'redux'
import reducerShots from './reducerShots'

const rootReducer = combineReducers({
  shots: reducerShots
})

export default rootReducer
