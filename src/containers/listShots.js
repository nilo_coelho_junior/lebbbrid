import React, {Component} from 'react'
import { connect } from 'react-redux'
import ListShotItem from './listShotsItem'
import { getShots } from '../actions/index'
import Loading from '../components/loading'

class ListShots extends Component {

  componentWillMount () {
    this.props.getShots()
  }

  renderShots () {
    if (this.props.loading) {
      return (<Loading />)
    } else {
      return (
        <ol className='list-unstyled'>
          {
            this.props.shots.map(shot =>
              <ListShotItem key={shot.id} shot={shot} />
            )
          }
        </ol>
      )
    }
  }

  render () {
    return (
      this.renderShots()
    )
  }
}

const mapStateToProps = (state) => {
  return {
    shots: state.shots.data,
    loading: state.shots.loadingList,
    error: state.shots.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getShots: (params) => {
      dispatch(getShots(params))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListShots)
