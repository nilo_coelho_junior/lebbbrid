import React, {Component} from 'react'
import { connect } from 'react-redux'
import ListShotItem from './listShotsItem'
import { getShots } from '../actions/index'
import Loading from '../components/loading'
import { Modal } from 'react-bootstrap'

class Shot extends Component {

  componentWillMount () {
    this.props.getShots()
  }

  renderShots () {
    if (this.props.loading) {
      return (<Loading />)
    } else {
      return (
        <ol className='list-unstyled'>
          {
            this.props.shots.map(shot =>
              <ListShotItem shot={shot} />
            )
          }
        </ol>
      )
    }
  }

  render () {
    return (
      <Modal show >
        <Modal.Body>
          sSASDASD
        </Modal.Body>
      </Modal>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    shots: state.shots.data,
    loading: state.shots.loadingList,
    error: state.shots.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getShots: (params) => {
      dispatch(getShots(params))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shot)
