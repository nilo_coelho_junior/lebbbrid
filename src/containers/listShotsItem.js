import React, {Component} from 'react'
import { Link } from 'react-router'
import { SHOTS } from '../constants/routeTypes'

export default class ListShotItem extends Component {

  description () {
    let description = this.props.shot.description

    if (description !== null) {
      description = description.replace(/(<([^>]+)>)/ig, '')
      if (description.length < 147) {
        return description
      } else {
        return description.substr(0, 147) + '...'
      }
    }
  }

  render () {
    return (
      <li className='col-sm-3 shot-container'>
        <Link to={
          {
            pathname: SHOTS + this.props.shot.id,
            state: {modal: true}
          }}>
          <div className='shot'>
            <div className='wrapper'>
              <div className='preview'>
                <a href='#' className='thumbnail'>
                  <img src={this.props.shot.images.normal} />
                </a>
              </div>
              <div className='description'>
                <strong className='title'>{this.props.shot.title}</strong>
                <span className='text'>{this.description()}</span>
                <span className='date'>July 25, 2016</span>
              </div>
            </div>
          </div>
          <h5 className='col-sm-12 shot-author'>
            <img src={this.props.shot.user.avatar_url} className='img-circle' />
            <span>{this.props.shot.user.name}</span>
          </h5>
        </Link>
      </li>
    )
  }
}
