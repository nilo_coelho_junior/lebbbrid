import React, {Component} from 'react'
import {FormGroup, FormControl, Button, InputGroup} from 'react-bootstrap'

export default class App extends Component {
  render () {
    return (
      <form className='col-sm-6 col-sm-push-3'>
        <FormGroup>
          <InputGroup>
            <FormControl type='text' placeholder='Busque no dribbble' />
            <InputGroup.Button>
              <Button bsStyle='primary'>
                Buscar
              </Button>
            </InputGroup.Button>
          </InputGroup>
        </FormGroup>
      </form>
    )
  }
}
