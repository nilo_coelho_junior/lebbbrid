export const ROOT_URL = 'http://localhost:9000/'
export const SHOTS_URL = 'http://localhost:9000/shots/'

export const LIST_SHOTS_LOADING = 'list_shots_loading'
export const LIST_SHOTS_ERROR = 'list_shots_error'
export const LIST_SHOTS_RECEIVE = 'list_shots_receive'

export const SHOT_LOADING = 'shot_loading'
export const SHOT_ERROR = 'shot_error'
export const SHOT_RECEIVE = 'shot_receive'
