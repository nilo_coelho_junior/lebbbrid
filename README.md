### Getting Started

Checkout this repo, install dependencies, then start the gulp process with the following:

```
> git clone https://nilo_coelho_junior@bitbucket.org/nilo_coelho_junior/lebbbrid.git
> cd lebbbrid
> npm install
> npm start
```
