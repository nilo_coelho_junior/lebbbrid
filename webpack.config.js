const path = require('path')
const nodeExternals = require('webpack-node-externals')

module.exports = [{
  entry: [
    path.resolve(__dirname, './style/style.scss'),
    './src/index.js'
  ],
  output: {
    path: __dirname,
    publicPath: '/',
    filename: 'bundle.js'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [{
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['react', 'es2015', 'stage-1']
      }
    },
    {
      test: /\.woff2?$|\.ttf$|\.eot$|\.svg$/,
      loader: 'file'
    },
    {
      test: /\.scss$/,
      // loader: 'style-loader!css-loader!'
      loaders: ['style-loader', 'css-loader', 'sass-loader']
    }]
    // {
    //   test: /\.scss$/,
    //   loaders: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
    // }]
  },
  resolve: {
    extensions: ['', '.js', '.jsx', '.css', '.scss', '.sass'],
    modulesDirectories: [
      'node_modules'
    ]
  },
  devServer: {
    historyApiFallback: true,
    contentBase: './'
  },
  sassLoader: {
    includePaths: [path.resolve(__dirname, './style/style')]
  }
}]
