var express = require('express')
var axios = require('axios')

var dribbble = axios.create({
  baseURL: 'https://api.dribbble.com/v1',
  headers: {'Authorization': 'Bearer 9e2e46311299101d1ed189142a764afded829b2e80a64ffe71d4ebc0e30d10bf'}
})

var app = express()

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')
  next()
})

app.get('/shots', (req, res) => {
  dribbble.get('/shots?' + req._parsedUrl.query).then(response => {
    res.send(response.data)
  }).catch(error => {
    console.log(error)
    res.send(error)
  })
})

app.listen(9000)
